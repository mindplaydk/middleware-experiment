<?php

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

interface HandlerInterface
{
    public function process(RequestInterface $request): ResponseInterface;
}
