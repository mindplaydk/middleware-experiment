<?php

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class HandlerProxy implements HandlerInterface
{
    /**
     * @var callable
     */
    private $callback;

    public function __construct(callable $callback)
    {
        $this->callback = $callback;
    }

    public function process(RequestInterface $request): ResponseInterface
    {
        return call_user_func($this->callback, $request);
    }
}
