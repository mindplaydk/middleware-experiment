<?php

class_alias("HandlerInterface", "DelegateInterface", true);

/**
 * Marker interface designating a Handler as the delegate in a middleware context.
 *
 * This would be part of the middleware standard.
 */
