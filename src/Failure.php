<?php

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class Failure implements HandlerInterface
{
    /**
     * @var int
     */
    private $index;

    public function __construct(int $index)
    {
        $this->index = $index;
    }

    public function process(RequestInterface $request): ResponseInterface
    {
        throw new RuntimeException("middleware stack exhausted (requested index: {$this->index})");
    }
}
