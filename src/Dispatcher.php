<?php

use mindplay\unbox\Container;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class Dispatcher implements HandlerInterface
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var string[]
     */
    private $stack;

    /**
     * @var HandlerInterface[]
     */
    private $cache = [];

    /**
     * @param Container $container DI container for resolution of middleware components
     * @param string[]  $stack     list of middleware component names
     */
    public function __construct(Container $container, array $stack)
    {
        $this->container = $container;
        $this->stack = $stack;
    }

    public function process(RequestInterface $request): ResponseInterface
    {
        return $this->resolve(0)->process($request);
    }

    private function resolve(int $index): HandlerInterface
    {
        if (! isset($this->cache[$index])) {
            if (! isset($this->stack[$index])) {
                return new Failure($index);
            }

            $this->cache[$index] = new HandlerProxy(function (ServerRequestInterface $request) use ($index): ResponseInterface {
                $this->cache[$index] = $this->container->create($this->stack[$index], [DelegateInterface::class => $this->resolve($index + 1)]);

                return $this->cache[$index]->process($request);
            });
        }

        return $this->cache[$index];
    }
}
