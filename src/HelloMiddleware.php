<?php

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\Response\HtmlResponse;

class HelloMiddleware implements HandlerInterface
{
    /**
     * @var DelegateInterface
     */
    private $delegate;

    public function __construct(DelegateInterface $delegate)
    {
        $this->delegate = $delegate;
    }

    public function process(RequestInterface $request): ResponseInterface
    {
        if ($request->getUri()->getPath() === "/hello") {
            return new HtmlResponse("<h1>Hello!</h1>");
        }

        return $this->delegate->process($request);
    }
}
