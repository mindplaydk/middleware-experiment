<?php

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\Response\HtmlResponse;

class NotFoundMiddleware implements HandlerInterface
{
    public function process(RequestInterface $request): ResponseInterface
    {
        return new HtmlResponse("<h1>404 Not Found</h1>", 404);
    }
}
