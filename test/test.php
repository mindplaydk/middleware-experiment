<?php

use mindplay\unbox\Container;
use mindplay\unbox\ContainerFactory;
use Zend\Diactoros\ServerRequest;

require dirname(__DIR__) . '/vendor/autoload.php';
require dirname(__DIR__) . '/src/DelegateInterface.php'; // dafuq?

test(
    'The simple way: no dispatcher required',
    function () {
        $app = new HelloMiddleware(
            new NotFoundMiddleware()
        );

        eq($app->process(new ServerRequest([], [], "http://localhost/hello", "GET"))->getStatusCode(), 200);
        eq($app->process(new ServerRequest([], [], "http://localhost/", "GET"))->getStatusCode(), 404);
    }
);

test(
    'Dynamically: with a DI container',
    function () {
        $factory = new ContainerFactory();

        $factory->register(HelloMiddleware::class);
        $factory->register(NotFoundMiddleware::class);

        $factory->set("app.stack", [
            HelloMiddleware::class,
            NotFoundMiddleware::class,
        ]);

        $factory->register(Dispatcher::class, function (Container $container) {
            return new Dispatcher($container, $container->get("app.stack"));
        });

        $container = $factory->createContainer();

        $container->call(function (Dispatcher $dispatcher) {
            eq(inspect($dispatcher, "cache"), [], "no components are aggressively initialized");

            eq($dispatcher->process(new ServerRequest([], [], "http://localhost/hello", "GET"))->getStatusCode(), 200);

            eq(array_map("get_class", inspect($dispatcher, "cache")), ["HelloMiddleware", "HandlerProxy"], "unused component remains proxied");

            eq($dispatcher->process(new ServerRequest([], [], "http://localhost/", "GET"))->getStatusCode(), 404);

            eq(array_map("get_class", inspect($dispatcher, "cache")), ["HelloMiddleware", "NotFoundMiddleware"], "both components now initialized");
        });
    }
);

exit(run());
